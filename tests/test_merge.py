import subprocess
from pathlib import Path
from tempfile import TemporaryDirectory

import pytest

from potodo.merge import get_msgmerge_command, sync_po_and_pot

try:
    subprocess.run([get_msgmerge_command()])
except FileNotFoundError:
    pytest.skip(
        "skipping tests in an environment without GNU gettext", allow_module_level=True
    )


def test_merges_file_in_main_directory(repo_dir):
    pots_dir = repo_dir.parent / "pots"
    with TemporaryDirectory() as tmp_dir:
        sync_po_and_pot(repo_dir, pots_dir, Path(tmp_dir))
        assert (
            Path(tmp_dir, "file1.po").read_text()
            == """#: /un/chemin/idiot.rst:69
msgid "This is an updated dummy sentence."
msgstr ""

#: /un/chemin/idiot.rst:666
msgid "We should translate this eventually"
msgstr ""

#~ msgid "This is a dummy sentence."
#~ msgstr "Ceci est une phrase bateau."

#, fuzzy
#~ msgid "Incredibly useful as a tool, this potodo"
#~ msgstr "Incroyablement inutile comme outil, ce potodo"

#~ msgid "Hello darkness my old friend"
#~ msgstr "Vous qui lisez cette ligne, vous tes trs beau."

#, fuzzy
#~ msgid "I am only there to make sure"
#~ msgstr "I don't get counted as a fuzzy entry"
"""
        )


def test_merges_a_file_in_a_subdirectory(repo_dir):
    pots_dir = repo_dir.parent / "pots"
    with TemporaryDirectory() as tmp_dir:
        sync_po_and_pot(repo_dir, pots_dir, Path(tmp_dir))
        assert (
            Path(tmp_dir, "folder/finished.po").read_text()
            == """#: /un/chemin/idiot.rst:420
msgid "Incredibly useful as a tool, this potodo"
msgstr "Incroyablement inutile comme outil, ce potodo"
"""
        )


def test_skips_po_file_without_pot(repo_dir):
    pots_dir = repo_dir.parent / "pots"
    with TemporaryDirectory() as tmp_dir:
        sync_po_and_pot(repo_dir, pots_dir, Path(tmp_dir))
        assert not Path(tmp_dir, "file2.po").exists()


def test_skips_po_file_without_pot_in_a_subdirectory(repo_dir):
    pots_dir = repo_dir.parent / "pots"
    with TemporaryDirectory() as tmp_dir:
        sync_po_and_pot(repo_dir, pots_dir, Path(tmp_dir))
        assert not Path(tmp_dir, "folder/file3.po").exists()


def test_moves_pot_as_po_when_no_po(repo_dir):
    pots_dir = repo_dir.parent / "pots"
    with TemporaryDirectory() as tmp_dir:
        sync_po_and_pot(repo_dir, pots_dir, Path(tmp_dir))
        assert (
            Path(tmp_dir, "file3.po").read_text()
            == """msgid "This string is in source, but not yet in the translation"
msgstr ""
"""
        )


def test_moves_pot_as_po_when_no_po_in_a_subdirectory(repo_dir):
    pots_dir = repo_dir.parent / "pots"
    with TemporaryDirectory() as tmp_dir:
        sync_po_and_pot(repo_dir, pots_dir, Path(tmp_dir))
        assert (
            Path(tmp_dir, "folder/file5.po").read_text()
            == """msgid "This string is in source in a subdirectory, but not yet in the translation"
msgstr ""
"""
        )
